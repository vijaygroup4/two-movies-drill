//function to return movies leonardo has acted
function leonardo(object) {
  let keys = Object.keys(object);
  //applying filter to catch movies leonard has acted
  let earningsKeys = keys.filter((key) => {
    return object[key].actors.includes("Leonardo Dicaprio") === true;
  });

  let earningsArray = earningsKeys.map((key) => {
    return {
      name: key,
      ...object[key],
    };
  });

  return earningsArray;
}

//exporting the above function
module.exports = leonardo;
