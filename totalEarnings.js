//function to return movies that has total earnings > $500M
function totalEarnings(object) {
  let keys = Object.keys(object);
  //filtering the movies which have total earnings > $500M
  let earningsKeys = keys.filter((key) => {
    return object[key].totalEarnings > "$500M";
  });

  let earningsArray = earningsKeys.map((key) => {
    return {
      name: key,
      ...object[key],
    };
  });

  return earningsArray;
}

//exporting the above function
module.exports = totalEarnings;
