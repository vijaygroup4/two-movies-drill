//function to group the movies
function groupMovies(object) {
  let keysArray = Object.keys(object);

  //applying reduce method to return grouped Object
  let groupedArray = keysArray.reduce(
    (accumulator, currentValue, index, array) => {
      //applying multiple if else statements to push the movie names into specific genre
      if (object[currentValue].genre.includes("drama")) {
        accumulator["drama"].push(currentValue);
      } else if (object[currentValue].genre.includes("sci-fi")) {
        accumulator["scifi"].push(currentValue);
      } else if (object[currentValue].genre.includes("adventure")) {
        accumulator["adventure"].push(currentValue);
      } else if (object[currentValue].genre.includes("thriller")) {
        accumulator["thriller"].push(currentValue);
      } else if (object[currentValue].genre.includes("crime")) {
        accumulator["crime"].push(currentValue);
      }

      return accumulator;
    },
    {
      drama: [],
      scifi: [],
      adventure: [],
      thriller: [],
      crime: [],
    }
  );

  return groupedArray;
}

//exporting the above function
module.exports = groupMovies;
