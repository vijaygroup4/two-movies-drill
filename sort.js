//compare function compares two movies at a time based on imdb rating,totalEarnings
function compare(movie1, movie2) {
  //converting the total earnings strings to numbers
  let earnings1 = parseFloat(movie1.totalEarnings.replace(/[$M]/g, ""));
  let earnings2 = parseFloat(movie2.totalEarnings.replace(/[$M]/g, ""));

  let difference = movie1.imdbRating - movie2.imdbRating;

  //if imdb ratings are equal,sorting happens on total earnings
  if (difference === 0) {
    return earnings1 - earnings2;
  }

  return difference;
}

//function to sort based on imdb rating and totalEarnings
function sort(object) {
  let keysArray = Object.keys(object);

  //obtaining the keys initially in ascending order
  let sortedMoviesKeys = keysArray.sort((key1, key2) => {
    return compare(object[key1], object[key2]);
  });

  let sortedMovies = sortedMoviesKeys.map((key) => {
    return {
      name: key,
      ...object[key],
    };
  });
  return sortedMovies;
}

//exporting the sort function
module.exports = sort;
