//function to return movies which have oscar nominations > 3 times and totalEarnings
//greater than $500M
function oscar(object) {
  let keys = Object.keys(object);
  //filtering the movies based on oscar nominations and total Earnings
  let earningsKeys = keys.filter((key) => {
    return (
      object[key].oscarNominations > 3 && object[key].totalEarnings > "$500M"
    );
  });

  let earningsArray = earningsKeys.map((key) => {
    return {
      name: key,
      ...object[key],
    };
  });

  return earningsArray;
}

//exporting the above function
module.exports = oscar;
